﻿using HumanManagement;

internal class Program
{
    private static void Main(string[] args)
    {
        Validation valid = new Validation();

        Console.WriteLine("Nhap so luong giao vien: ");
        if (int.TryParse(Console.ReadLine(), out int n) || n <= 0)
        {
            Console.WriteLine("Nhap lai so luong giao vien: ");
        }
        
        GiaoVien[] list = new GiaoVien[n];
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine($"Nhap thong tin cho giao vien thu {i + 1}:\n");
            GiaoVien giaoVien = new GiaoVien();

            
                Console.Write("Ho ten: ");
                giaoVien.HoTen = valid.CheckString("Ho ten: ", giaoVien.HoTen);
            

            Console.Write("Nam sinh: ");
            giaoVien.NamSinh = int.Parse(Console.ReadLine());

            Console.Write("Luong co ban: ");
            giaoVien.LuongCoBan = double.Parse(Console.ReadLine()); 

            Console.Write("He so luong: " );
            giaoVien.HeSoLuong = double.Parse(Console.ReadLine());
            Console.WriteLine("");

            list[i] = giaoVien;

        }
        Console.WriteLine("----------------------------------------");
        Console.WriteLine("\nDanh sach giao vien:");
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine($"Giao vien thu {i + 1}:");
            list[i].XuatThongTin();
        }

        Console.WriteLine("\nGiao vien co luong thap nhat: ");
        for(int i = 0; i < n; i++)
        {
            if (list[i].TinhLuong() < list[0].TinhLuong())
            {
                list[0] = list[i];
            }
            
        }
        list[0].XuatThongTin();
        
    }
}