﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanManagement
{
    public class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }
        public GiaoVien() { }
        public GiaoVien(string hoten, int namsinh, double luongcoban, double hesoluong) : base(hoten, namsinh, luongcoban, hesoluong)
        {
            HeSoLuong = hesoluong;
        }
        public void NhapThongTin(double HeSoLuong)
        {
            HeSoLuong = HeSoLuong;
        }
        public double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }
        public void XuatThongTin()
        {
            Console.WriteLine($"\nHo ten: {HoTen} | nam sinh: {NamSinh} | luong co ban: {LuongCoBan} |  he so luong: {HeSoLuong} | luong: {TinhLuong()} VND \n ");
        }
        public double XuLy()
        {
            return HeSoLuong + 0.6;
        }
                
        

        

    }
}
