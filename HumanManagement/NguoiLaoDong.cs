﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanManagement
{
    public abstract class NguoiLaoDong
    {
        public string HoTen { get; set;}
        public int NamSinh { get; set; }
        public double LuongCoBan { get; set; }
        public double HeSoLuong { get; set; }

        protected NguoiLaoDong() { }

        protected NguoiLaoDong(string hoten, int namsinh, double luongcoban, double heSoLuong)
        {
            HoTen = hoten;
            NamSinh = namsinh;
            LuongCoBan = luongcoban;
            HeSoLuong = heSoLuong;
        }
        public void NhapThongTin(string hoten, int namsinh, double luongcoban, double heSoLuong)
        {
            HoTen = hoten;
            NamSinh = namsinh;
            LuongCoBan = luongcoban;

        }
        public void Nhapthongtin(string hoten, int namsinh, double luongcoban, double heSoLuong)
        {
            HoTen = hoten;
            NamSinh = namsinh;
            LuongCoBan = luongcoban;

        }
        public double TinhLuong()
        {
            return LuongCoBan;
        }
        public void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}");
        }


    }
}
