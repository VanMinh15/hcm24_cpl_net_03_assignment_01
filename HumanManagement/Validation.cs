﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanManagement
{
    public class Validation
    {
        
        public static int CheckIntLimit(string msg, int min, int max)
        {
            int result;
            while (true)
            {
                Console.Write(msg);
                bool isInt = int.TryParse(Console.ReadLine(), out result);
                if (isInt && result >= min && result <= max)
                {
                    break;
                }
                Console.WriteLine($"Please input number in range {min} - {max}");
            }
            return result;
        }

        public static double CheckDoubleLimit(string msg, double min, double max)
        {
            double result;
            while (true)
            {
                Console.Write(msg);
                bool isDouble = double.TryParse(Console.ReadLine(), out result);
                if (isDouble && result >= min && result <= max)
                {
                    break;
                }
                Console.WriteLine($"Please input number in range {min} - {max}");
            }
            return result;
        }

        public string CheckString(string msg, string hovaten)
        {
            string result;
            if(string.IsNullOrWhiteSpace(hovaten))
            {
                while (true)
                {
                    Console.Write(msg);
                    result = Console.ReadLine();
                    if (!string.IsNullOrWhiteSpace(result) && !result.Any(char.IsNumber))
                    {
                        break;
                    }
                    Console.WriteLine("Sai format, hay nhap lai!");
                }
            }
            else
            {
                result = hovaten;
            }
            return result;
        }
    }
}
